<?php

class RolesTableSeeder extends Seeder {

	public function run()
	{
        Role::create([
            'name' => 'owner',
            'description' => 'Site owners group'
        ]);

        Role::create([
            'name' => 'admin',
            'description' => 'Site Administrators group'
        ]);
        Role::create([
            'name' => 'user',
            'description' => 'Site clients group'
        ]);
	}

}
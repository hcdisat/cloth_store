<?php namespace DisatCorp\Storage\Repositories;


interface RepositoryInterface {

    function getAll();
    function get($id);
    function getAllByFields(array $fields);

    function update();
    function delete($id);
}
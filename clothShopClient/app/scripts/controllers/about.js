'use strict';

/**
 * @ngdoc function
 * @name clothShopClientApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the clothShopClientApp
 */
angular.module('clothShopClientApp')
  .controller('AboutController', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name clothShopClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the clothShopClientApp
 */
angular.module('clothShopClientApp')
  .controller('MainController', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

(function(){ 'use strict'

  var controllerDependencies = [
    '$scope',
    RegistrationController
  ];

  angular.module('clothShopClientApp')
    .controller('RegistrationController', controllerDependencies);


  function RegistrationController($scope){

    $scope.saveUser = function(user){
      console.log(user);
    };
  }

}());

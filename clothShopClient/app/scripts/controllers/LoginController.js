(function(){ 'use strict'

  var controllerDependencies = [
    '$scope',
    LoginController
  ];

  angular.module('clothShopClientApp')
    .controller('LoginController', controllerDependencies);


  function LoginController($scope){

    $scope.sendLoginData = function(user){
      console.log(user);
    };
  }

}());
